import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  standalone: true,
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
  imports: [CommonModule]
})
export class HomeComponent {

  navItems = [
    {
      title: "ACCUEIL"
    },
    {
      title: "PRESENTATION"
    },
    {
      title: "TADD-ART"
    },
    {
      title: "SPONSORSHIP"
    },
    {
      title: "BLOG"
    },
    {
      title: "GALERIE"
    },
    {
      title: "CONTACT"
    },
  ]


  currentIndex = 0;
  items = [
    { imageUrl: 'https://themes.themesbrand.com/finder/angular/img/real-estate/catalog/03.jpg' },
    { imageUrl: 'https://themes.themesbrand.com/finder/angular/img/real-estate/catalog/04.jpg' },
    { imageUrl: 'https://themes.themesbrand.com/finder/angular/img/real-estate/catalog/05.jpg' },
    { imageUrl: 'https://themes.themesbrand.com/finder/angular/img/real-estate/catalog/06.jpg' },
    { imageUrl: 'https://themes.themesbrand.com/finder/angular/img/real-estate/catalog/07.jpg' },
  ];

  prev() {
    this.currentIndex = this.currentIndex > 0 ? this.currentIndex - 1 : this.items.length - 1;
  }

  next() {
    this.currentIndex = this.currentIndex < this.items.length - 1 ? this.currentIndex + 1 : 0;
  }



}
